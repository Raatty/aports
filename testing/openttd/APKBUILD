# Contributor: Adrian Siekierka <kontakt@asie.pl>
# Maintainer: Thomas Kienlen <kommander@laposte.net>
pkgname=openttd
pkgver=12.0
pkgrel=0
pkgdesc="Open source version of the Transport Tycoon Deluxe simulator"
url="https://www.openttd.org"
arch="all"
license="GPL-2.0-or-later"
makedepends="
	cmake
	fontconfig-dev
	fluidsynth-dev
	freetype-dev
	icu-dev
	libpng-dev
	lzo-dev
	sdl-dev
	xz-dev
	zlib-dev
	"
subpackages="$pkgname-doc $pkgname-lang::noarch"
source="https://cdn.openttd.org/openttd-releases/$pkgver/openttd-$pkgver-source.tar.xz"
options="!check" # TODO: find how to start test suite, no check implemented

build() {
	cmake -B build \
		-DCMAKE_INSTALL_BINDIR=bin \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_DATADIR=share/games \
		-DGLOBAL_DATA_DIR=share/games/openttd \
		-DCMAKE_BUILD_TYPE=None \
		-DReleaseBuild=ON
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

lang() {
	pkgdesc="$pkgdesc (localizations)"

	cd "$pkgdir"
	mkdir -p "$subpkgdir"/usr/share/games/openttd/lang
	find usr/share/games/openttd/lang/ ! -name 'english.lng' -type f \
		-exec mv {} "$subpkgdir"/{} \;
}

sha512sums="
a1042fe52892cf301d3a2c7e972d9c091829638119d2ca9b55165a6b5568660f178e808db4b0bf2d3e4538dbdaea7b0683d4de9333defbcd95e109d591c05a78  openttd-12.0-source.tar.xz
"
